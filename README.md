# Kamino Airgap Bundler

## Helm Chart and OCI Image Bundler

This tool automates the process of downloading Helm charts along with their dependencies and associated OCI images. It packages these assets into a structured bundle suitable for deployment in air-gapped or restricted environments.

### Features

- Download Helm Charts: Automatically fetch Helm charts and their recursive dependencies.
- Extract OCI Images: Identify and save all associated container images required by the Helm charts.
- Package into Bundles: Create a single compressed tarball containing both Helm charts and images, organized for easy deployment.

### Requirements

- Python 3.6+
- Helm 3.x
- Docker
- Access to Helm repositories and Docker registries (as specified in configuration).

### Setup

- Clone the repository to your local machine.
- Ensure Python, Helm, and Docker are installed and correctly configured on your system.
- Set up an environment variable OUTPUT_DIR to specify the output directory for bundles, or it will default to ./<bundle_name>_bundle.

### Configuration

Configuration is managed through a JSON file, which specifies the charts to download and their sources. Here’s an example structure for `charts_config.json`:

```json
{
    "bundle_name": "example_bundle",
    "charts": [
      {
        "name": "kube-prometheus-stack",
        "version": "40.1.0",
        "repo": "prometheus-community",
        "repo_url": "https://prometheus-community.github.io/helm-charts"
      },
      {
        "name": "keycloak",
        "version": "21.0.1",
        "repo": "bitnami",
        "repo_url": "https://charts.bitnami.com/bitnami"
      }
    ]
  }
  
```

### Usage

Run the script from the command line by providing the path to your configuration file:

```bash
python main.py -f path/to/some/charts_config.json
```

The script will perform the following actions:

- Add the specified Helm repositories.
- Download the specified Helm charts and update their dependencies.
- Extract all OCI images referenced in these charts.
- Save the images and package everything into a `tar.gz` file located in the specified OUTPUT_DIR.

### Output

The output will be a `tar.gz` file containing:

- A helm directory with all the packaged Helm charts.
- An oci directory with tar files of all the extracted images.