import json
import os
import subprocess
import tarfile
import argparse

def run_shell_command(command):
    """ Execute shell command and return the output. """
    result = subprocess.run(command, shell=True, text=True, capture_output=True)
    if result.returncode != 0:
        raise Exception("Command failed: {}".format(result.stderr))
    return result.stdout.strip()

def download_and_package_charts(charts, helm_dir):
    """ Download and package each specified Helm chart along with its dependencies. """
    for chart in charts:
        print(f"Adding repo: {chart['repo']} URL: {chart['repo_url']}")
        run_shell_command(f"helm repo add {chart['repo']} {chart['repo_url']}")
        print(f"Updating dependencies for chart: {chart['name']}")
        # Pull the chart first to a local directory
        local_chart_path = run_shell_command(f"helm pull {chart['repo']}/{chart['name']} --version {chart['version']} --untar --destination ./temp_charts")
        # Update dependencies, which downloads them into 'charts/' directory inside the chart's directory
        run_shell_command(f"helm dependency update ./temp_charts/{chart['name']}")
        # Package the chart with dependencies into helm_dir
        run_shell_command(f"helm package ./temp_charts/{chart['name']} --destination {helm_dir}")
        # Clean up the temporary chart directory
        run_shell_command(f"rm -rf ./temp_charts/{chart['name']}")


def extract_and_save_images(charts, helm_dir, oci_dir):
    """ Extract and save images from packaged Helm charts. """
    for chart in charts:
        chart_tgz = os.path.join(helm_dir, f"{chart['name']}-{chart['version']}.tgz")
        print(f"Extracting images from chart: {chart_tgz}")
        output = run_shell_command(f"helm template {chart_tgz}")
        images = set()
        # not all charts follow best practices, add some additional checks
        for line in output.split('\n'):
            if "image:" in line:
                parts = line.split('"')
                if len(parts) > 1:
                    images.add(parts[1])
                else:
                    # Attempt to extract image from a different format, e.g., using single quotes or without quotes
                    parts = line.split("image:")[1].strip().split()
                    if parts:
                        image = parts[0].strip().strip('"').strip("'")
                        if image:
                            images.add(image)
        for image in images:
            print(f"Pulling image: {image}")
            run_shell_command(f"docker pull {image}")
            image_file = os.path.join(oci_dir, image.replace('/', '_') + ".tar")
            print(f"Saving image to {image_file}")
            run_shell_command(f"docker save -o {image_file} {image}")


def create_bundle(output_dir):
    """ Create a tarball of the entire output directory. """
    with tarfile.open(f"{output_dir}.tar.gz", "w:gz") as tar:
        tar.add(output_dir, arcname=os.path.basename(output_dir))
    print(f"Created bundle: {output_dir}.tar.gz")

def main(config_file):
    config_basename = os.path.basename(config_file).replace('.json', '')
    output_dir = os.getenv('OUTPUT_DIR', f'./{config_basename}_bundle')
    helm_dir = os.path.join(output_dir, 'helm')
    oci_dir = os.path.join(output_dir, 'oci')
    
    os.makedirs(helm_dir, exist_ok=True)
    os.makedirs(oci_dir, exist_ok=True)
    
    with open(config_file, 'r') as file:
        config = json.load(file)

    # Download, package charts, and handle dependencies
    download_and_package_charts(config['charts'], helm_dir)
    
    # Extract and save images from charts
    extract_and_save_images(config['charts'], helm_dir, oci_dir)

    # Package everything into a tar.gz
    create_bundle(output_dir)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Package Helm charts and images for air-gapped deployment.')
    parser.add_argument('--config-file', '-f', type=str, help='Path to the configuration JSON file.')
    args = parser.parse_args()
    main(args.config_file)
